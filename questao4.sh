#!/bin/bash
#
cpu=$(lscpu)
echo "informações da CPU:"
echo "$cpu"

memoria=$(free -h)
echo "informações de memoria:"
echo "$memoria"

disco=$(df -h)
echo "informações de disco:"
echo "$disco"

placa_video=$(lspci | grep VGA)
echo "informações da placa de video:"
echo "$placa_video"

placa_rede=$(lspci | grep Ethernet)
echo "informações de placa de rede:"
echo "$placa_rede"
